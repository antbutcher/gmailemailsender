﻿namespace EmailSender.App
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = new SenderConfiguration
            {
                FilePath = GetSetting("HtmlFilePath"),
                GmailEmail = GetSetting("Gmail.User"),
                GmailPass = GetSetting("Gmail.Password"),
                ToAddress = GetSetting("Email.DeliverTo"),
                Subject = GetSetting("Email.Subject")
            };

            Sender.EmailFile(config);
        }

        static string GetSetting(string key)
        {
            return System.Configuration.ConfigurationManager.AppSettings[key];
        }
    }
}
