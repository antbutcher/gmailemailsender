﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EmailSender.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public bool Sending { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            btnSelectFile.Click += BtnSelectFile_Click;

            btnSend.Click += BtnSend_Click;
        }

        private void BtnSend_Click(object sender, RoutedEventArgs e)
        {
            if(this.Sending)
            {
                return;
            }

            this.Sending = true;
            btnSend.IsEnabled = false;

            txtError.Text = "";
            txtSuccess.Text = "";

            var config = new SenderConfiguration
            {
                FilePath = txtFilePath.Text,
                GmailEmail = txtUserName.Text,
                GmailPass = txtPassword.Password,
                ToAddress = txtTo.Text,
                Subject = txtSubject.Text
            };

            try
            {
                Sender.EmailFile(config);
                txtSuccess.Text = "Sent!";
            }
            catch(Exception ex)
            {
                txtError.Text = ex.Message;
            }
            finally
            {
                this.Sending = false;
                btnSend.IsEnabled = true;
            }
        }

        private void BtnSelectFile_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.OpenFileDialog();
            var result = dialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                txtFilePath.Text = dialog.FileName;
            }
        }
    }
}
