﻿using System.IO;
using System.Net;
using System.Net.Mail;

namespace EmailSender
{
    public class SenderConfiguration
    {
        public string FilePath { get; set; }
        public string GmailEmail { get; set; }
        public string GmailPass { get; set; }
        public string ToAddress { get; set; }
        public string Subject { get; set; }
    }

    public class Sender
    {
        public static void EmailFile(SenderConfiguration config)
        {
            var contents = Sender.GetContents(config.FilePath);

            Sender.Send(contents, config);
        }

        private static string GetContents(string path)
        {
            return File.ReadAllText(path);
        }

        private static void Send(string contents, SenderConfiguration config)
        {
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(config.GmailEmail, config.GmailPass)
            };

            using (var message = new MailMessage(config.GmailEmail, config.ToAddress)
            {
                IsBodyHtml = true,
                Subject = config.Subject,
                Body = contents
            })
            {
                smtp.Send(message);
            }
        }
    }
}
